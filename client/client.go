package main

 import (
         "log"
         "net"
         "fmt"
         "os"
         "bufio"
         "strings"
 )

 const (
   JOIN_REQ = "JOIN_REQ"
   PASS_REQ = "PASS_REQ"
   PASS_ACCEPT = "PASS_ACCEPT"
   REJECT = "REJECT"
   TERMINATE = "TERMINATE"
   DATA = "DATA"
 )

 func main() {
         hostName := "localhost"
         portNum := "6000"

         service := hostName + ":" + portNum

         addr, err := net.ResolveUDPAddr("udp", service)

         conn, err := net.DialUDP("udp", nil, addr)

         if err != nil {
              log.Fatal(err)
         }

         fmt.Printf("Established connection to %s \n", service)

         defer conn.Close()

         sendMessageToServer(JOIN_REQ, conn, addr)

         buffer := make([]byte, 1024)
         n, _, err := conn.ReadFromUDP(buffer)

         if err != nil {
            log.Fatal(err)
         }

         result := strings.TrimRight(string(buffer[:n]), "\n")

         handleServerResponse(result, conn)

 }

 func checkPassword(conn *net.UDPConn) {

   reader := bufio.NewReader(os.Stdin)
   fmt.Print("Enter password: ")
   text, _ := reader.ReadString('\n')
   fmt.Println(text)

   // write a message to server
   message := []byte(text)

   _, err := conn.Write(message)

   if err != nil {
      log.Println(err)
   }

   buffer := make([]byte, 1024)
   n, _, err := conn.ReadFromUDP(buffer)

   if err != nil {
      log.Fatal(err)
   }

   result := strings.TrimRight(string(buffer[:n]), "\n")

   handleServerResponse(result, conn)
 }

 func readFile(conn *net.UDPConn) {
   buffer := make([]byte, 1024)
   n, _, err := conn.ReadFromUDP(buffer)

   if err != nil {
      log.Fatal(err)
   }

   result := strings.TrimRight(string(buffer[:n]), "\n")

   fmt.Printf("Received file %s \n", result)

 }

 func sendMessageToServer(message string, conn *net.UDPConn, addr *net.UDPAddr) {
   messageToSend := []byte(message)
   _, err := conn.Write(messageToSend)
   if err != nil {
      log.Fatal(err)
   }
 }

 func handleServerResponse(response string, conn *net.UDPConn) {

   fmt.Printf("Response from server: %s \n", response)

   if response == PASS_REQ {
      checkPassword(conn)
   } else if response == PASS_ACCEPT {
      readFile(conn)
   } else if response == REJECT {
      conn.Close()
   } else if response == TERMINATE {
     conn.Close()
   }
 }
