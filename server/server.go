package main

 import (
         "fmt"
         "log"
         "net"
         "strings"
         "io"
         "os"
 )

 const (
   JOIN_REQ = "JOIN_REQ"
   PASS_REQ = "PASS_REQ"
   PASS_ACCEPT = "PASS_ACCEPT"
   REJECT = "REJECT"
   TERMINATE = "TERMINATE"
   DATA = "DATA"

   // Password shouldn't be hard coded of course
   // Will implement the hashing in later assignments I believe
   CORRECT_PASSWORD = "aseem"
   MAX_PASSWORD_TRIES = 3
 )

 func main() {
    hostName := "localhost"
    portNum := "6000"
    service := hostName + ":" + portNum

    udpAddr, err := net.ResolveUDPAddr("udp4", service)

    handleError(err)

    ln, err := net.ListenUDP("udp", udpAddr)

    handleError(err)

    defer ln.Close()

    for {
        handleUDPConnection(ln)
    }

 }

 func handleUDPConnection(conn *net.UDPConn) {

    fmt.Println("handling UDP connection")

    buffer := make([]byte, 1024)
    n, addr, err := conn.ReadFromUDP(buffer)
    handleError(err)

    fmt.Println("receiving response from UDP")

    response := strings.TrimRight(string(buffer[:n]), "\n")
    fmt.Println(response)

    handleClientResponse(response, conn, addr)

    conn.Close()
 }

 func checkPassword(conn *net.UDPConn, maximumTries int) {

    buffer := make([]byte, 1024)
    n, addr, err := conn.ReadFromUDP(buffer)

    handleError(err)

    if strings.TrimRight(string(buffer[:n]), "\n") == CORRECT_PASSWORD {
        fmt.Println("correct password")
        sendMessageToClient(PASS_ACCEPT, conn, addr)
        sendFileToClient("myfile.rtf", conn, addr)
        sendMessageToClient(TERMINATE, conn, addr)
    } else {
        fmt.Println("incorrect password")
        maximumTries--
        if maximumTries == 0 {
            sendMessageToClient(REJECT, conn, addr)
            return
        }

        sendMessageToClient(PASS_REQ, conn, addr)
        checkPassword(conn, maximumTries)
    }
 }

 func sendFileToClient(fileName string, conn *net.UDPConn, addr *net.UDPAddr) {

    file, err := os.Open(fileName)
    handleError(err)

    sendBuffer := make([]byte, 1024)
    fmt.Println("Sending file")

    for {
        _, err = file.Read(sendBuffer)
        if err == io.EOF {
            break
        }

        conn.WriteToUDP(sendBuffer, addr)
    }

 }

 func sendMessageToClient(message string, conn *net.UDPConn, addr *net.UDPAddr) {
    messageToSend := []byte(message)
    conn.WriteToUDP(messageToSend, addr)
 }

 func handleClientResponse(response string, conn *net.UDPConn, addr *net.UDPAddr) {

   fmt.Printf("Response from client: %s \n", response)

   if response == JOIN_REQ {
     sendMessageToClient(PASS_REQ, conn, addr)
     checkPassword(conn, MAX_PASSWORD_TRIES)
   }
 }

 func handleError(err error) {
   if err != nil {
       log.Fatal(err)
   }
 }
